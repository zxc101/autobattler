using System.Collections.Generic;
using UnityEngine;

public static class GameHelper
{
    private static System.Random random = new System.Random();

    public static void SetAlpha(this SpriteRenderer spriteRenderer, float alpha)
    {
        var c = spriteRenderer.color;
        spriteRenderer.color = new Color(c.r, c.g, c.b, alpha);
    }

    public static Color SetAlpha(this Color c, float alpha)
    {
        return new Color(c.r, c.g, c.b, alpha);
    }

    public static Vector3 AppendZ(this Transform transform, float z = .0f)
    {
        var pos = new Vector3(transform.position.x, transform.position.y, z);
        transform.position = pos;
        return pos;
    }

    public static Vector3 AppendZ(this Vector3 vector, float z = .0f)
    {
        return new Vector3(vector.x, vector.y, z);
    }

    public static T Random<T>(this List<T> list)
    {
        var val = list[UnityEngine.Random.Range(0, list.Count)];
        return val;
    }

    /// First element's chance make the least 
    public static T RandomByChance<T>(this List<T> list) where T : IRandom
    {
        var total = .0f;
        var probs = new float[list.Count];

        for(var i = 0; i < probs.Length; i++)
        {
            probs[i] = list[i].Chance;
            total += probs[i];
        }

        var randomPoint = (float)random.NextDouble() * total;

        for (var i = 0; i < probs.Length; i++)
        {
            if (randomPoint < probs[i]) return list[i];
            randomPoint -= probs[i];
        }

        return list[0];
    }

    public static int NearestIndex(this Vector3[] nodes, Vector3 destination)
    {
        float min = Mathf.Infinity;
        int index = 0;
        for (int i = 0; i < nodes.Length; i++)
        {
            min = Mathf.Min(Vector3.Distance(destination, nodes[i]), min);
            if (min == Vector3.Distance(destination, nodes[i]))
            {
                index = i;
            }
        }

        return index;
    }

    public static int NearestIndex(this List<Transform> nodes, Vector3 destination)
    {
        float min = Mathf.Infinity;
        int index = 0;
        for (int i = 0; i < nodes.Count; i++)
        {
            min = Mathf.Min(Vector3.Distance(destination, nodes[i].position), min);
            if (min == Vector3.Distance(destination, nodes[i].position))
            {
                index = i;
            }
        }
        return index;
    }

    public static Vector3 NearestPosition(this Vector3[] nodes, Vector3 destination)
    {
        return nodes[NearestIndex(nodes, destination)];
    }

    public static Vector3 NearestPosition(this List<Transform> nodes, Vector3 destination)
    {
        float min = Mathf.Infinity;
        int index = 0;
        for(int i = 0; i < nodes.Count; i++)
        {
            min = Mathf.Min(Vector3.Distance(destination, nodes[i].position), min);
            if(min == Vector3.Distance(destination, nodes[i].position))
            {
                index = i;
            }
        }
        return nodes[index].position;
    }

    public static Vector3 Center(this Transform[] points)
    {
        Vector3 finalPos = Vector3.zero;
        for(var i = 0; i < points.Length; i++)
        {
            finalPos += points[i].position;
        }

        finalPos /= points.Length;
        return finalPos;
    }
}
