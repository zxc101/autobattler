using UnityEngine;

[System.Serializable]
public class DataRandom<T> : IRandom
{
    public float chance;

    public float Chance
    {
        get
        {
            return chance;
        }
    }

    public T root;
}
