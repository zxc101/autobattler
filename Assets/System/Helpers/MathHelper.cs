using UnityEngine;

public static class MathHelper
{
    public static float Sin(this float angle)
    {
        if(angle > 0)
        {
            angle %= 360;
            return angle == 90 ? 1 : angle == 180 ? 0 : angle == 270 ? -1 : Mathf.Sin(angle * Mathf.Deg2Rad);
        }
        else
        {
            angle %= 360;
            angle *= -1;
            return angle == 90 ? -1 : angle == 180 ? 0 : angle == 270 ? 1 : Mathf.Sin(angle * Mathf.Deg2Rad);
        }
    }

    public static float Cos(this float angle)
    {
        if (angle > 0)
        {
            angle %= 360;
            return angle == 90 ? 0 : angle == 180 ? -1 : angle == 270 ? 0 : Mathf.Cos(angle * Mathf.Deg2Rad);
        }
        else
        {
            angle %= 360;
            angle *= -1;
            return angle == 90 ? 0 : angle == 180 ? -1 : angle == 270 ? 0 : Mathf.Cos(angle * Mathf.Deg2Rad);
        }
    }
}
