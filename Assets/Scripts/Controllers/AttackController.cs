using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackController
{
    public IEnumerator Attack(float power, float interval, params Transform[] targets)
    {
        for(int i = 0; i < targets.Length; i++)
        {
            if (targets[i].GetComponent<IHealth>() != null)
            {
                targets[i].GetComponent<IHealth>().GetDamage(power);
            }
        }
        yield return new WaitForSeconds(interval);
    }
}
