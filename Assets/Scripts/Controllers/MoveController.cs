using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveController
{
    public Transform Transform => _transform;
    private Transform _transform;
    public Vector2 Speed => _speed;
    private Vector2 _speed;

    private Vector2 _standartSpeed;

    public MoveController(Transform transform)
    {
        _transform = transform;
    }

    public IEnumerator Move(Vector2 speed)
    {
        _speed = speed;
        _standartSpeed = speed;
        _transform.Translate(speed * Time.fixedDeltaTime);
        yield return new WaitForSeconds(Time.deltaTime);
    }

    public IEnumerator Impulse(Vector2 impulseSpeed, float minSpeed, float accelerator, float smooth)
    {
        while (Vector2.Distance(_speed, impulseSpeed) > Mathf.Abs(minSpeed))
        {
            yield return ChangeSpeed(impulseSpeed, minSpeed, accelerator, smooth);
        }
        while (Vector2.Distance(_speed, _standartSpeed) > Mathf.Abs(minSpeed))
        {
            yield return ChangeSpeed(_standartSpeed, minSpeed, accelerator * 10, smooth);
        }
    }

    private IEnumerator ChangeSpeed(Vector2 newSpeed, float minSpeed, float accelerator, float smooth)
    {
        while (Vector2.Distance(_speed, newSpeed) > Mathf.Abs(minSpeed))
        {
            _speed = Vector2.Lerp(_speed, newSpeed, Time.fixedDeltaTime * accelerator / smooth);
            _transform.Translate(_speed * Time.fixedDeltaTime);
            yield return new WaitForSeconds(Time.fixedDeltaTime);
        }
        _speed = newSpeed;
    }
}
