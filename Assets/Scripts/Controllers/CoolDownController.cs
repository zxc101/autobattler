using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoolDownController
{
    public float Impulse => _impulse;
    private float _impulse;

    public float Shield => _shield;
    private float _shield;

    public float Heal => _heal;
    private float _heal;

    private float _standartImpulse;
    private float _standartShield;
    private float _standartHeal;

    public CoolDownController(SHeroSkills model)
    {
        _standartImpulse = model.impulse.coolDown;
        _standartShield = model.shield.coolDown;
        _standartHeal = model.heal.coolDown;
    }

    public IEnumerator Update()
    {
        while(true)
        {
            _impulse = _impulse > 0 ? _impulse - Time.deltaTime : 0;
            _shield = _shield > 0 ? _shield - Time.deltaTime : 0;
            _heal = _heal > 0 ? _heal - Time.deltaTime : 0;

            yield return new WaitForSeconds(Time.deltaTime);
        }
    }

    public void StartImpulse()
    {
        _impulse = _standartImpulse;
    }

    public void StartShield()
    {
        _shield = _standartShield;
    }

    public void StartHeal()
    {
        _heal = _standartHeal;
    }
}
