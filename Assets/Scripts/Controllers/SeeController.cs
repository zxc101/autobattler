using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeeController
{
    private Transform _seePoint;
    private Vector2 _hitDir;
    //private List<Transform> _targets = new List<Transform>();

    //public Transform Target => _targets.Count > 0 ? _targets[0] : null;
    //public List<Transform> Targets => _targets;

    public SeeController(Transform seePoint)
    {
        _seePoint = seePoint;
    }

    public void Update<T>(float distance, ref List<Transform> _targets)
    {
        _targets.Clear();
        _hitDir.x = _seePoint.localEulerAngles.z.Cos();
        _hitDir.y = _seePoint.localEulerAngles.z.Sin();
        RaycastHit2D[] hits = Physics2D.RaycastAll(_seePoint.position, _hitDir, distance);
        for (int i = 0; i < hits.Length; i++)
        {
            if(hits[i].transform.GetComponent<T>() != null)
            {
                _targets.Add(hits[i].transform);
            }
        }
    }
}
