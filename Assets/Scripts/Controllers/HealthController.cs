public class HealthController
{
    public float HP => _hp;
    public float _hp;
    public float Armor => _armor;
    public float _armor;
    public float EnergyShield => _energyShield;
    public float _energyShield;

    public HealthController(float hp, float armor, float energyShield)
    {
        _hp = hp;
        _armor = armor;
        _energyShield = energyShield;
    }

    public HealthController(float hp, float armor)
    {
        _hp = hp;
        _armor = armor;
    }

    public HealthController(float hp)
    {
        _hp = hp;
    }

    public bool GetDamage(float power)
    {
        if (HP == 0 && Armor == 0 && EnergyShield == 0) return false;

        if(EnergyShield >= power)
        {
            _energyShield -= power;
        }
        else if(EnergyShield > 0)
        {
            power -= EnergyShield;
            _energyShield = 0;
            if(power > 0)
            {
                if (Armor >= power)
                {
                    _armor -= power;
                }
                else if (Armor > 0)
                {
                    power -= Armor;
                    _armor = 0;
                    if (power > 0)
                    {
                        if (HP >= power)
                        {
                            _hp -= power;
                        }
                        else
                        {
                            _hp = 0;
                        }
                    }
                }
                else if (HP >= power)
                {
                    _hp -= power;
                }
                else
                {
                    _hp = 0;
                }
            }
        }
        else if (Armor >= power)
        {
            _armor -= power;
        }
        else if (Armor > 0)
        {
            power -= Armor;
            _armor = 0;
            if (power > 0)
            {
                if (HP >= power)
                {
                    _hp -= power;
                }
                else
                {
                    _hp = 0;
                }
            }
        }
        else if (HP >= power)
        {
            _hp -= power;
        }
        else
        {
            _hp = 0;
        }
        return true;
    }

    public System.Collections.IEnumerator ActivateShield(float energyShield, float time)
    {
        _energyShield = energyShield;
        yield return new UnityEngine.WaitForSeconds(time);
        _energyShield = 0;
    }

    public void Heal(float heal)
    {
        _hp += heal;
    }
}
