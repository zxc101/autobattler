using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventarController
{
    public int Energy => _energy;
    private int _energy;
    public int HealBoxes => _healBoxes;
    private int _healBoxes;
    public int Impulse => _impulse;
    private int _impulse;
    public int Shield => _shield;
    private int _shield;

    public InventarController(SInventar inventar, SHeroSkills skills)
    {
        _energy = inventar.energy;
        _healBoxes = skills.heal.resource;
        _impulse = skills.impulse.resource;
        _shield = skills.shield.resource;
    }

    public bool UseHeal()
    {
        if (_healBoxes > 0)
        {
            _healBoxes--;
            return true;
        }
        else
        {
            return false;
        }
    }

    public bool UseImpulse()
    {
        return UseEnergy(_impulse);
    }

    public bool UseShield()
    {
        return UseEnergy(_shield);
    }

    private bool UseEnergy(int spendEnergy)
    {
        if(_energy >= spendEnergy)
        {
            _energy -= spendEnergy;
            return true;
        }
        else
        {
            return false;
        }
    }
}
