using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Actor : MonoBehaviour
{
    protected Transform _transform;

    private bool _isTick;

    protected virtual IEnumerator DoSomething() { yield return new WaitForSeconds(Time.deltaTime); }
    protected virtual void UpdateState() { }

    public void Awake()
    {
        _transform = transform;
    }

    protected virtual void Init() { }

    public void StartTick()
    {
        if (!_isTick)
        {
            _isTick = true;
            StartCoroutine(Tick());
        }
    }

    public void StopTick()
    {
        if (_isTick)
        {
            _isTick = false;
        }
    }

    public IEnumerator Tick()
    {
        while (_isTick)
        {
            yield return DoSomething();
            UpdateState();
        }
    }

    private void OnEnable()
    {
        StartTick();
    }

    private void OnDisable()
    {
        StopTick();
    }
}
