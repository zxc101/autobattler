using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Hero : Actor, IHealth
{
    protected EActorState state;

    [SerializeField] private Transform seePoint;

    [Header("UI")]
    [SerializeField] private Text textEnergyPoints;
    [SerializeField] private Text textHealthPoints;

    private HealthController healthController;
    private InventarController inventarController;
    private SeeController seeController;
    private MoveController moveController;
    private AttackController attackController;
    private CoolDownController coolDownController;

    private HeroModel model;

    private List<Transform> _targets = new List<Transform>();
    private Transform Target => _targets.Count > 0 ? _targets[0] : null;
    private List<Transform> Targets => _targets;

    public new void Awake()
    {
        base.Awake();
        state = EActorState.Move;

        model = DataBase.Instance.GetRandomHero();

        healthController = new HealthController(model.health.hp, model.armor.shield, model.energyShield.shield);
        inventarController = new InventarController(model.inventar, model.skills);
        seeController = new SeeController(seePoint);
        moveController = new MoveController(transform);
        attackController = new AttackController();
        coolDownController = new CoolDownController(model.skills);

        textEnergyPoints.text = inventarController.Energy.ToString();
        textHealthPoints.text = inventarController.HealBoxes.ToString();

        StartTick();
        StartCoroutine(coolDownController.Update());
    }

    protected override IEnumerator DoSomething()
    {
        switch (state)
        {
            case EActorState.Move:
                yield return moveController.Move(model.move.speed);
                break;
            case EActorState.Attack:
                yield return attackController.Attack(model.attack.power, model.attack.interval, Target);
                break;
            default:
                yield break;
        }
    }

    protected override void UpdateState()
    {
        seeController.Update<IHealth>(model.attack.distance, ref _targets);
        if (Target)
        {
            state = EActorState.Attack;
            return;
        }

        state = EActorState.Move;
    }

    public void GetDamage(float power)
    {
        healthController.GetDamage(power);
        print($"Hero: EnergyShield: {healthController.EnergyShield} | Armor: {healthController.Armor} | HP: {healthController.HP}");
    }

    public void ActivateImpulse()
    {
        if(coolDownController.Impulse == 0)
        {
            if (inventarController.UseImpulse())
            {
                for (int i = 0; i < Targets.Count; i++)
                {
                    Targets[i]?.GetComponent<IImpulse>().Impulse();
                }
                coolDownController.StartImpulse();
                textEnergyPoints.text = inventarController.Energy.ToString();
            }
            else
            {
                print("Not enought energy for Impulse");
            }
    }
        else
        {
            print($"CoolDown: Impulse: {coolDownController.Impulse}");
        }
    }

    public void ActivateShield()
    {
        if (coolDownController.Shield == 0)
        {
            if (inventarController.UseShield())
            {
                StartCoroutine(healthController.ActivateShield(model.energyShield.shieldPoints, model.energyShield.time));
                coolDownController.StartShield();
                textEnergyPoints.text = inventarController.Energy.ToString();
            }
            else
            {
                print("Not enought energy for Shield");
            }
        }
        else
        {
            print($"CoolDown: Shield: {coolDownController.Shield}");
        }
    }

    public void ActivateHeal()
    {
        if (coolDownController.Heal == 0)
        {
            if(inventarController.UseHeal())
            {
                healthController.Heal(model.health.healPoints);
                coolDownController.StartHeal();
                textHealthPoints.text = inventarController.HealBoxes.ToString();
            }
            else
            {
                print("Not enought HealBoxes");
            }
        }
        else
        {
            print($"CoolDown: Heal: {coolDownController.Heal}");
        }
    }
}
