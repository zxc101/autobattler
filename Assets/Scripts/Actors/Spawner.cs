using System.Collections;
using UnityEngine;

public sealed class Spawner : Actor
{
    public Transform prefab;
    public float interval;
    public int count;

    protected override void Init()
    {
    }

    protected override IEnumerator DoSomething()
    {
        for (int i = 0; i < count; i++)
        {
            Instantiate(prefab, _transform.position, Quaternion.identity);
            yield return new WaitForSeconds(interval);
        }
        StopTick();
    }

    protected override void UpdateState()
    {
    }
}
