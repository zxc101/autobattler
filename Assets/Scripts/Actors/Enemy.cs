using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : Actor, IHealth, IImpulse
{
    public Transform seePoint;

    private HealthController healthController;
    private SeeController seeController;
    private MoveController moveController;
    private AttackController attackController;

    private EnemyModel model;

    private bool isImpulse;
    private EActorState _state;
    private List<Transform> _targets = new List<Transform>();
    private Transform Target => _targets.Count > 0 ? _targets[0] : null;
    private List<Transform> Targets => _targets;

    public new void Awake()
    {
        base.Awake();
        _state = EActorState.Move;

        model = DataBase.Instance.GetRandomEnemy();

        healthController = new HealthController(model.health.hp, model.armor.shield);
        seeController = new SeeController(seePoint);
        moveController = new MoveController(transform);
        attackController = new AttackController();

        StartTick();
    }

    protected override IEnumerator DoSomething()
    {
        switch (_state)
        {
            case EActorState.Impulse:
                yield return moveController.Impulse(model.impulse.impulseSpeed,
                                                    model.impulse.minSpeed,
                                                    model.impulse.accelerator,
                                                    model.impulse.smooth);
                isImpulse = false;
                _state = EActorState.Move;
                break;
            case EActorState.Move:
                yield return moveController.Move(model.move.speed);
                break;
            case EActorState.Attack:
                yield return attackController.Attack(model.attack.power, model.attack.interval, Target);
                break;
            default:
                yield break;
        }
    }

    protected override void UpdateState()
    {
        if (isImpulse)
        {
            _state = EActorState.Impulse;
            return;
        }
        seeController.Update<Hero>(model.attack.distance, ref _targets);
        if (Target)
        {
            _state = EActorState.Attack;
            return;
        }
        _state = EActorState.Move;
    }

    public void GetDamage(float power)
    {
        healthController.GetDamage(power);
        //print($"Enemy: Shield2: {healthController.Shield2} | Shield1: {healthController.Shield1} | HP: {healthController.HP}");
    }

    public void Impulse()
    {
        isImpulse = true;
    }
}
