using System.Collections;
using UnityEngine;

public interface IAttack
{
    float Power { get; }
    float Distance { get; }
    float Interval { get; }
    IEnumerator Attack(Transform damager, params Transform[] targets);
}
