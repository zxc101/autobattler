using System.Collections.Generic;
using UnityEngine;

public interface ISee
{
    Transform Target { get; }
    List<Transform> Targets { get; }
    void UpdateTargets<T>(float distance);
}
