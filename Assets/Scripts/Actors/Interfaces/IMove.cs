using System.Collections;
using UnityEngine;

public interface IMove
{
    Vector2 Speed { get; set; }
    IEnumerator Move(Transform transform);
}
