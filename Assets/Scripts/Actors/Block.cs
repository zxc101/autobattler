using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block : Actor, IHealth
{
    private HealthController healthController;

    private BlockModel model;

    public new void Awake()
    {
        base.Awake();
        model = DataBase.Instance.GetRandomBlock();
        healthController = new HealthController(model.health.hp);
    }

    public void GetDamage(float power) { }

    private void OnMouseDown()
    {
        if (!healthController.GetDamage(1))
        {
            Destroy(gameObject);
            LootDB.Instance.Plus(model.resources);
        }
    }
}
