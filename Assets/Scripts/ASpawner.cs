using System.Collections;
using UnityEngine;

public abstract class ASpawner : MonoBehaviour
{
    protected Transform _transform;

    protected abstract IEnumerator Spawn();

    void Awake()
    {
        _transform = transform;
    }

    private void Start()
    {
        StartCoroutine(Spawn());
    }
}
