using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LootDB : Single<LootDB>
{
    public SResources Resources => _model.resources;
    public ResourcesModel _model;

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }

    public void Plus(SResources resources)
    {
        _model.resources += resources;
    }

    public void Minus(SResources resources)
    {
        _model.resources -= resources;
    }

    public bool IsEquels(SResources resources)
    {
        return _model.resources == resources;
    }
}
