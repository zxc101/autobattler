using UnityEngine;

[CreateAssetMenu(menuName = "Model/Hero", fileName = "NewHeroModel")]
public class HeroModel : ScriptableObject
{
    public SHealth health;
    public SArmor armor;
    public SEnergyShield energyShield;
    public SInventar inventar;
    public SSee see;
    public SMove move;
    public SAttack attack;
    public SHeroSkills skills;
}
