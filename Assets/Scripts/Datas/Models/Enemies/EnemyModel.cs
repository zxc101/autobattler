using UnityEngine;

[CreateAssetMenu(menuName = "Model/Enemy", fileName = "NewEnemyModel")]
public class EnemyModel : ScriptableObject
{
    public SMove move;
    public SHealth health;
    public SArmor armor;
    public SAttack attack;
    public SSee see;
    public SImpulse impulse;
}
