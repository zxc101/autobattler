using UnityEngine;

[CreateAssetMenu(menuName = "Game/Resources", fileName = "Resources")]
public class ResourcesModel : ScriptableObject
{
    public SResources resources;
}
