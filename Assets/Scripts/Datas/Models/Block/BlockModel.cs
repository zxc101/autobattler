using UnityEngine;

[CreateAssetMenu(menuName = "Model/Block", fileName = "NewBlockModel")]
public class BlockModel : ScriptableObject
{
    public SHealth health;
    public SResources resources;
}
