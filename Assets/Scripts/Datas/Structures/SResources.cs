[System.Serializable]
public class SResources
{
    public int people;
    public int money;
    public int crystals;
    public int energy;

    public SResources()
    {
        people = 0;
        money = 0;
        crystals = 0;
        energy = 0;
    }

    public SResources(int People, int Money, int Crystals, int Energy)
    {
        people = People;
        money = Money;
        crystals = Crystals;
        energy = Energy;
    }

    public static SResources operator +(SResources v1, SResources v2)
    {
        return new SResources(v1.people + v2.people,
                               v1.money + v2.money,
                               v1.crystals + v2.crystals,
                               v1.energy + v2.energy);
    }

    public static SResources operator -(SResources v1, SResources v2)
    {
        return new SResources(v1.people - v2.people,
                               v1.money - v2.money,
                               v1.crystals - v2.crystals,
                               v1.energy - v2.energy);
    }

    public static bool operator ==(SResources v1, SResources v2)
    {
        return v1.people == v2.people &&
               v1.money == v2.money &&
               v1.crystals == v2.crystals &&
               v1.energy == v2.energy;
    }

    public static bool operator !=(SResources v1, SResources v2)
    {
        return v1.people != v2.people ||
               v1.money != v2.money ||
               v1.crystals != v2.crystals ||
               v1.energy != v2.energy;
    }

    public override bool Equals(object obj)
    {
        return base.Equals(obj);
    }

    public override int GetHashCode()
    {
        return base.GetHashCode();
    }

    public override string ToString()
    {
        return string.Format("People: {0}, Money: {1}, Crystals: {2}, Energy: {3}", people, money, crystals, energy);
    }
}
