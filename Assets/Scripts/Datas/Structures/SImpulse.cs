using UnityEngine;

[System.Serializable]
public class SImpulse
{
    public Vector2 impulseSpeed;
    public float minSpeed;
    public float accelerator;
    public float smooth;
}
