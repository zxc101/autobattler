using UnityEngine;

[System.Serializable]
public class SAttack
{
    public float power;
    public float distance;
    public float interval;
}
