using System.Collections.Generic;
using UnityEngine;

public class DataBase : Single<DataBase>
{
    [SerializeField] private List<DataRandom<HeroModel>> heroes;
    [SerializeField] private List<DataRandom<EnemyModel>> enemies;
    [SerializeField] private List<DataRandom<BlockModel>> blocks;

    public HeroModel GetHero(int index)
    {
        return heroes[index].root;
    }

    public EnemyModel GetEnemy(int index)
    {
        return enemies[index].root;
    }

    public BlockModel GetBlock(int index)
    {
        return blocks[index].root;
    }

    public HeroModel GetRandomHero()
    {
        return heroes.RandomByChance().root;
    }

    public EnemyModel GetRandomEnemy()
    {
        return enemies.RandomByChance().root;
    }

    public BlockModel GetRandomBlock()
    {
        return blocks.RandomByChance().root;
    }
}
